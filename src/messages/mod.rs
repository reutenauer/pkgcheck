mod fatald;
mod errorsd;
mod warningsd;
mod informationd;

use self::fatald::*;
use self::errorsd::*;
use self::warningsd::*;
use self::informationd::*;




macro_rules! no_colors {
    () => {
        $crate::ARGS.no_colors
    };
}

macro_rules! error_occured {
    () => {
        $crate::ERROR_OCCURED.store(true, Ordering::Relaxed);
    };
}
macro_rules! yellow {
    ($fmt:expr) => {
        if no_colors!() {
            $fmt.clear()
        } else {
            $fmt.bright_yellow()
        }
    };
}

macro_rules! red {
    ($fmt:expr) => {
        if no_colors!() {
            $fmt.clear()
        } else {
            $fmt.bright_red()
        }
    };
}

macro_rules! msgid {
    ($fmt:expr) => {
        match (no_colors!(), &$fmt[..1]) {
            (true, _) => $fmt.clear(),
            (false, "E") => $fmt.bright_red().bold(),
            (false, "I") => $fmt.bright_yellow(),
            (false, "W") => $fmt.bright_red(),
            (false, "F") => $fmt.bright_red().bold(),
            (_, _) => $fmt.clear(),
        }
    };
}



macro_rules! f0001 {
    () => {
        error_occured!();
        print!(
            "{}   Specify a directory to check (use option -d)\n",
            msgid!("F0001")
        );
    };
}


macro_rules! f0002 {
    ($fmt1:expr) => {
        error_occured!();
        print!(
            "{}   Specified directory {} does not exist. Exiting...\n",
            msgid!("F0002"),
            $fmt1
        );
    };
}


macro_rules! f0003 {
    ($fmt1:expr) => {
        error_occured!();
        print!(
            "{}   Specified TDS archive {} does not exist or is no file. Exiting...\n",
            msgid!("F0003"),
            $fmt1
        );
    };
}


macro_rules! f0004 {
    ($fmt1:expr) => {
        error_occured!();
        print!(
            "{}   File {} specified as TDS archive is no zip archive. Exiting...\n",
            msgid!("F0004"),
            $fmt1
        );
    };
}

macro_rules! f0005 {
    ($fmt1:expr) => {
        error_occured!();
        print!(
            "{}   Bad filename {} for the tds.zip archive. Exiting...\n",
            msgid!("F0005"),
            $fmt1
        );
    };
}

macro_rules! f0006 {
    ($fmt1:expr) => {
        error_occured!();
        print!(
            "{}   Unknown error code `{}` specified with option -e resp. ---explain. Exiting...\n",
            msgid!("F0006"),
            $fmt1
        );
    };
}

macro_rules! e0001 {
    ($fmt1:expr, $fmt2:expr, $fmt3:expr) => {
        error_occured!();
        print!(
            "{}   Bad character {} ({:#02x}) found in file name {} at offset {}\n",
            msgid!("E0001"),
            $fmt1,
            $fmt1 as usize,
            $fmt2,
            $fmt3
        );
    };
}

macro_rules! e0002 {
    ($fmt1:expr, $fmt2:expr) => {
        error_occured!();
        print!(
            "{}   File {} has bad permissions: {}\n",
            msgid!("E0002"),
            $fmt1,
            $fmt2
        );
    };
}

macro_rules! e0003 {
    ($fmt:expr) => {
        error_occured!();
        print!("{}   {} is no text file\n", msgid!("E0003"), $fmt);
    };
}

macro_rules! e0004 {
    ($fmt:expr) => {
        error_occured!();
        print!("{}   Empty directory {} detected\n", msgid!("E0004"), $fmt);
    };
}

macro_rules! e0005 {
    ($fmt:expr) => {
        error_occured!();
        print!("{}   Empty file {} detected\n", msgid!("E0005"), $fmt);
    };
}

macro_rules! e0006 {
    ($fmt:expr) => {
        error_occured!();
        print!("{}   Hidden directory {} detected\n", msgid!("E0006"), $fmt);
    };
}

macro_rules! e0007 {
    ($fmt:expr) => {
        error_occured!();
        print!("{}   Hidden file {} detected\n", msgid!("E0007"), $fmt);
    };
}

macro_rules! e0008 {
    ($fmt:expr) => {
        error_occured!();
        print!("{}   Temporary file {} detected\n", msgid!("E0008"), $fmt);
    };
}

macro_rules! e0009 {
    () => {
        error_occured!();
        print!(
            "{}   One of README/README.md/README.txt must exist\n",
            msgid!("E0009")
        );
    };
}

macro_rules! e0010 {
    ($fmt:expr) => {
        error_occured!();
        print!("{}   {} is a broken symlink\n", msgid!("E0010"), $fmt);
    };
}

macro_rules! e0011 {
    ($fmt1:expr, $fmt2:expr) => {
        error_occured!();
        print!(
            "{}   Directory {} has bad permissions: {}\n",
            msgid!("E0011"),
            $fmt1,
            $fmt2
        );
    };
}

macro_rules! e0012 {
    ($fmt:expr) => {
        error_occured!();
        print!("{}   CRLF detected: {}\n", msgid!("E0012"), $fmt);
    };
}

macro_rules! e0013 {
    ($fmt:expr) => {
        error_occured!();
        print!("{}   {} is a socket special file\n", msgid!("E0013"), $fmt);
    };
}

macro_rules! e0014 {
    ($fmt:expr) => {
        error_occured!();
        print!("{}   {} is a fifo file\n", msgid!("E0014"), $fmt);
    };
}

macro_rules! e0015 {
    ($fmt:expr) => {
        error_occured!();
        print!("{}   {} is a block device file\n", msgid!("E0015"), $fmt);
    };
}

macro_rules! e0016 {
    ($fmt:expr) => {
        error_occured!();
        print!(
            "{}   {} is a character device file\n",
            msgid!("E0016"),
            $fmt
        );
    };
}

macro_rules! e0017 {
    ($fmt:expr) => {
        error_occured!();
        print!("{}   PDF error detected in {}\n", msgid!("E0017"), $fmt);
    };
}

macro_rules! e0018 {
    ($fmt:expr) => {
        error_occured!();
        print!(
            "{}   Unwanted directory {} detected\n",
            msgid!("E0018"),
            $fmt
        );
    };
}

macro_rules! e0019 {
    ($fmt1:expr, $fmt2:expr) => {
        error_occured!();
        print!(
            "{}   {} generated by {} exists\n",
            msgid!("E0019"),
            $fmt1,
            $fmt2
        );
    };
}

macro_rules! e0020 {
    ($fmt:expr) => {
        error_occured!();
        print!(
            "{}   The top level directory of a TDS archive should not contain {}\n",
            msgid!("E0020"),
            $fmt
        );
    };
}

macro_rules! e0021 {
    ($fmt1:expr, $fmt2:expr, $fmt3:expr) => {
        error_occured!();
        print!(
            "{}   File {} : Error reading in line {}: {}\n",
            msgid!("E0021"),
            $fmt1,
            $fmt2,
            $fmt3
        );
    };
}

macro_rules! e0022 {
    ($fmt1:expr, $fmt2:expr) => {
        error_occured!();
        print!("{}   {}: `{}`\n", msgid!("E0022"), $fmt1, $fmt2);
    };
}

macro_rules! e0023 {
    ($fmt:expr) => {
        error_occured!();
        print!("{}   {}\n", msgid!("E0023"), $fmt);
    };
}

macro_rules! e0024 {
    ($fmt1:expr, $fmt2:expr) => {
        error_occured!();
        print!(
            "{}   The TDS zip archive {} has bad permissions: {}\n",
            msgid!("E0024"),
            $fmt1,
            $fmt2
        );
    };
}

macro_rules! e0025 {
    ($fmt:expr) => {
        error_occured!();
        print!(
            "{}   Duplicate names when ignoring letter case for: {}\n",
            msgid!("E0025"),
            $fmt
        );
    };
}

macro_rules! e0026 {
    ($fmt:expr) => {
        error_occured!();
        print!(
            "{}   {} : file not in TDS or different in TDS and non-install tree\n",
            msgid!("E0026"),
            $fmt
        );
    };
}

macro_rules! e0027 {
    ($fmt1:expr, $fmt2:expr) => {
        error_occured!();
        print!(
            "{}   {}: An I/O error occured -> {}\n",
            msgid!("E0027"),
            $fmt1,
            $fmt2
        );
    };
}



macro_rules! e0033 {
    ($fmt1:expr, $fmt2:expr) => {
        error_occured!();
        print!(
            "{}   Error `{}` when unpacking tds archive {}. Exiting...\n",
            msgid!("E0033"),
            $fmt2,
            $fmt1
        );
    };
}


macro_rules! w0001 {
    ($fmt:expr) => {
        print!(
            "{}   Archive as package file detected: {}\n",
            msgid!("W0001"),
            $fmt
        );
    };
}

macro_rules! w0002 {
    () => {
        print!(
            "{}   Duplicate files detected\n",
            msgid!("W0002"),
        );
    };
}


macro_rules! w0003 {
    ($fmt:expr) => {
        error_occured!();
        print!(
            "{}   Same named files detected in the package directory tree: {}\n",
            msgid!("W0003"),
            $fmt
        );
    };
}


macro_rules! i0002 {
    ($fmt:expr) => {
        print!(
            "{}   {} {}\n",
            msgid!("I0002"),
            yellow!("Checking package files in directory"),
            yellow!($fmt)
        );
    };
}

macro_rules! i0003 {
    ($fmt:expr) => {
        print!(
            "{}   {} {}\n",
            msgid!("I0003"),
            yellow!("Checking TDS zip archive"),
            yellow!($fmt)
        );
    };
}

macro_rules! i0004 {
    ($fmt:expr) => {
        print!(
            "{}   Correcting line endings for file {}\n",
            msgid!("I0004"),
            $fmt
        );
    };
}

macro_rules! i0005 {
    ($fmt:expr) => {
        print!(
            "{}   Correcting permissions for {}\n",
            msgid!("I0005"),
            $fmt
        );
    };
}

macro_rules! i0006 {
    () => {
        print!(
            "{}   Files having one of the following file name endings are regarded as temporary\n",
            msgid!("I0006")
        );
    };
}

macro_rules! i0007 {
    ($fmt:expr) => {
        print!(
            "{}   {}: Successfully converted from CRLF to LF\n",
            msgid!("I0007"), $fmt
        );
    };
}

pub fn explains(err: &str) {
    let err_upp = err.to_ascii_uppercase();
    match err_upp.as_str() {
        "F0001" => f0001d(),
        "F0002" => f0002d(),
        "F0003" => f0003d(),
        "F0004" => f0004d(),
        "F0005" => f0005d(),
        "F0006" => f0006d(),

        "E0001" => e0001d(),
        "E0002" => e0002d(),
        "E0003" => e0003d(),
        "E0004" => e0004d(),
        "E0005" => e0005d(),
        "E0006" => e0006d(),
        "E0007" => e0007d(),
        "E0008" => e0008d(),
        "E0009" => e0009d(),
        "E0010" => e0010d(),
        "E0011" => e0011d(),
        "E0012" => e0012d(),
        "E0013" => e0013d(),
        "E0014" => e0014d(),
        "E0015" => e0015d(),
        "E0016" => e0016d(),
        "E0017" => e0017d(),
        "E0018" => e0018d(),
        "E0019" => e0019d(),
        "E0020" => e0020d(),
        "E0021" => e0021d(),
        "E0022" => e0022d(),
        "E0023" => e0023d(),
        "E0024" => e0024d(),
        "E0025" => e0025d(),
        "E0026" => e0026d(),
        "E0027" => e0027d(),
        "E0033" => e0033d(),

        //        "I0001" => i0001d!(),
        "I0001" => i0001d(),
        "I0002" => i0002d(),
        "I0003" => i0003d(),
        "I0004" => i0004d(),
        "I0005" => i0005d(),
        "I0006" => i0006d(),

        "W0001" => w0001d(),
        "W0002" => w0002d(),
        "W0003" => w0003d(),

        e => println!("F0006   Unknown error code `{}` specified with option -e resp. --explain. Exiting...", e)
    }
}

pub fn explains_all() {
    explains("F0001");
    explains("F0002");
    explains("F0003");
    explains("F0004");
    explains("F0005");
    explains("F0006");

    explains("E0001");
    explains("E0002");
    explains("E0003");
    explains("E0004");
    explains("E0005");
    explains("E0006");
    explains("E0007");
    explains("E0008");
    explains("E0009");
    explains("E0010");
    explains("E0011");
    explains("E0012");
    explains("E0013");
    explains("E0014");
    explains("E0015");
    explains("E0016");
    explains("E0017");
    explains("E0018");
    explains("E0019");
    explains("E0020");
    explains("E0021");
    explains("E0022");
    explains("E0023");
    explains("E0024");
    explains("E0025");
    explains("E0026");
    explains("E0027");
    explains("E0033");

    explains("I0001");
    explains("I0002");
    explains("I0003");
    explains("I0004");
    explains("I0005");
    explains("I0006");

    explains("W0001");
    explains("W0002");
    explains("W0003");
}
