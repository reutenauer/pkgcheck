## I0006 -- Files having one of the following file name endings are regarded as temporary

Option --show-temp-endings was used, and pkgcheck prints a list of temporary 
file endings and their meanings.

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#noauxfiles
