## I0004 -- Correcting line endings for file

The file had CRLF line ending and will be corrected to have LF (Unix like) line endings.

For more details refer to:
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#crlf
