## E0005 -- Empty files not allowed 

Empty files are considered as rubbish, and are usually not accepted as
part of a package. 


For more details refer to: 
http://mirror.ctan.org/help/ctan/CTAN-upload-addendum.html#noemptyfiles
