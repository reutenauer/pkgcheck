## W0001 -- Archive as package file detected

Usually a CTAN package should not contain archives. An exception are situations where, for example,
the source code of a package is kept in a separate zip archive.
