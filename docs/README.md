
# Sources of message explanations 

Explanations of messages are in files

- EnnnnD.md for error messages
- WnnnnD.md for warning messages 

# Creating LaTeX file for including in `pkgcheck.tex`

`pandoc` is used to convert the markdows files to a LaTeX 
source which gets included in `pkgcheck.tex`

  pandoc -f gfm -t latex e*d.md -o errors.tex
  pandoc -f gfm -t latex w*d.md -o warnings.tex
  
  
# `crea_msg.pl` is used to create the Rust source files 

   ./crea_msg.pl -o ../src/messages/errorsd.rs    e*d.md
   ./crea_msg.pl -o ../src/messages/warningsd.rs  w*d.md
 
  Please note, that crea_msg.pl calls `pandoc` to convert the markdown files to text before
  crea_msg.pl writes out the Rust functions. 


