## E0033 -- Error when unpacking tds archive

In order to investigate the contents of the TDS zip archive `pkgcheck`
unpacks the TDS zip archive to a temporary location which failed for
the reason given in the error message.
