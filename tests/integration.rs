extern crate assert_cmd;
extern crate predicates;
extern crate escargot;

#[cfg(test)]
mod integration {
    use assert_cmd::prelude::*;
    use predicates::prelude::*;
    use std::path::Path;
    use std::process::Command;
    use std::fs;
    use std::fs::File;
    use std::io::prelude::*;


    fn sort_string_by_lines(s: &str) -> String {

        let mut lvec = Vec::new();
        let lines = s.split('\n');
        for l in lines {
            lvec.push(l);
        }
        lvec.sort_unstable();
        let lvec1 = lvec.iter().map(|v| format!("{}\n",v)).collect::<String>();

        // skip the last newline
        //lvec1[1..lvec1.len()-1].to_string()
        lvec1[1..lvec1.len()].to_string()
    }

    fn compare(output: &str, expect_file: &str) -> bool {
        true
    }
    // returns true if file is a regular file and does exist
    // returns false otherwise
    fn exists_file(file: &str) -> bool {
        match fs::metadata(file) {
            Ok(attr) => attr.is_file(),
            Err(_) => false,
        }
    }

    fn run_script(script: &str) {
        let _ = Command::new("sh")
            .arg("-c")
            .arg(script)
            .output()
            .expect("failed to execute process");
    }

    fn file_contents(file: &str) -> String {

        let fhdl = File::open(&file); 
        match fhdl {
            Ok(mut f) => {
                let mut buf = String::new();
                match f.read_to_string(&mut buf) {
                    Ok(_bytes_read) =>  {
                        return buf;
                    },
                    Err(e) => panic!("Read error")
                }
            },
            Err(e) => panic!("Error reading file {}: {:?}", file, e)
        }
    }


    fn main_binary() -> String  {
        let runner = ::escargot::CargoBuild::new()
            .current_release()
            .run().unwrap();
        runner.path().display().to_string()
    }

    macro_rules! error_tests {
        ($($name:ident,)*) => {
            $(
                #[test]
                fn $name() {
                    let e = stringify!($name);
                    let pkg_dir = &format!("tests/{}/somepkg", e);
                    let tds_zip_archive = &format!("tests/{}/somepkg.tds.zip", e);
                    let expected_data_file = &format!("tests/{}/expected.data.sorted", e);
                    let arguments =
                        match e {
                            "e0020" | "e0024" =>
                                vec!["--no-colors", "-d", pkg_dir, "-T", tds_zip_archive ],
                            "e0022" =>
                                vec!["--no-colors", "--urlcheck", "-d", pkg_dir ],
                            _ =>
                                vec!["--no-colors", "-d", pkg_dir]
                        };

                    let before = &format!("tests/{}/before.sh", e);
                    if exists_file(before) {
                        run_script(before);
                    }

                    let output = Command::new(main_binary())
                        .args(&arguments)
                        .output()
                        .expect("failed to execute process");

                    let sorted = sort_string_by_lines(&String::from_utf8_lossy(&output.stdout).to_string());


                    assert_eq!(&sorted, &file_contents(expected_data_file));

                    let after = &format!("tests/{}/after.sh", e);
                    if exists_file(after) {
                        run_script(after);
                    }
                }
            )*
        }
    }

    error_tests!{
        e0001,
        e0002,
        e0003,
        e0004,
        e0005,
        e0006,
        e0007,
        e0008,
        e0009,
        e0010,
        e0011,
        e0012,
        e0013,
        e0014,
        // e0015: we skip that test because we need root to create a block device file
        // e0016: we skip that test because we need root to create a character device file
        e0017,
        e0018,
        e0019,
        e0019_1,
        e0019_2,
        e0020,
        e0021,
        e0022,
        e0022a,   // actually we have a stale link but here we don't check it
        e0023,
        e0024,
        e0025,
    }

    macro_rules! ok_tests {
        ($($name:ident,)*) => {
            $(
                #[test]
                fn $name() {
                    // let e = stringify!($name);
                    // let pkg_dir = &format!("tests/{}/somepkg", e);
                    // let tds_zip_archive = &format!("tests/{}/somepkg.tds.zip", e);
                    // let expected_data = &format!("tests/{}/expected.data", e);
                    // let arguments =
                    //     if e == "ok002" {
                    //         vec!["--no-colors", "-d", pkg_dir, "-T", tds_zip_archive ]
                    //     } else {
                    //         vec!["--no-colors", "-d", pkg_dir]
                    //     };

                    // Command::main_binary()
                    //     .expect("singe-binary package that compiles")
                    //     .args(&arguments)
                    //     .assert()
                    //     .success()   // we have always success
                    //     .stdout(predicate::path::eq_file(Path::new(expected_data)));


                    let e = stringify!($name);
                    let pkg_dir = &format!("tests/{}/somepkg", e);
                    let tds_zip_archive = &format!("tests/{}/somepkg.tds.zip", e);
                    let expected_data_file = &format!("tests/{}/expected.data.sorted", e);
                    let arguments =
                        if e == "ok002" {
                            vec!["--no-colors", "-d", pkg_dir, "-T", tds_zip_archive ]
                        } else {
                            vec!["--no-colors", "-d", pkg_dir]
                        };


                    let before = &format!("tests/{}/before.sh", e);
                    if exists_file(before) {
                        run_script(before);
                    }

                    let output = Command::new(main_binary())
                        .args(&arguments)
                        .output()
                        .expect("failed to execute process");

                    let sorted = sort_string_by_lines(&String::from_utf8_lossy(&output.stdout).to_string());


                    assert_eq!(&sorted, &file_contents(expected_data_file));

                    let after = &format!("tests/{}/after.sh", e);
                    if exists_file(after) {
                        run_script(after);
                    }

                }
            )*
        }
    }

    ok_tests!{
        ok001,
        ok002,
        w0001,
        w0002,
    }

    #[test]
    fn pkg004_perm_toplevel_dir_e0011_e0023_e0009() {
        // Command::main_binary()
        //     .expect("singe-binary package that compiles")
        //     .args(&["--no-colors", "-d", "tests/pkg004/somepkg" ])
        //     .assert()
        //     .code(1) // or .success()
        //     .stdout(predicate::path::eq_file(Path::new("tests/pkg004/expected.data.sorted")));

        let e = "pkg004";
        let expected_data_file = &format!("tests/{}/expected.data.sorted", e);

        let before = &format!("tests/{}/before.sh", e);
        if exists_file(before) {
            run_script(before);
        }

        let output = Command::new(main_binary())
            .args(&["--no-colors", "-d", "tests/pkg004/somepkg" ])
            .output()
            .expect("failed to execute process");

        let sorted = sort_string_by_lines(&String::from_utf8_lossy(&output.stdout).to_string());


        assert_eq!(&sorted, &file_contents(expected_data_file));

        let after = &format!("tests/{}/after.sh", e);
        if exists_file(after) {
            run_script(after);
        }
    }

    #[test]
    fn f0002() {
        Command::main_binary()
            .expect("singe-binary package that compiles")
            .args(&["--no-colors", "-d", "tests/not_existing/somepkg"])
            .assert()
            .code(1)
            .stdout(predicate::path::eq_file(Path::new("tests/f0002/expected.data")));
    }

    // #[test]
    // fn tds_errors() {
    //     for (dir, pkg, tds_zip) in
    //         vec![ ("tests/tds_not_existing", "somepkg", "somepkg.tds.zip"),
    //                 ( "tests/tds_wrong_name" , "somepkg", "somepkg.zip")] {
    //             let d_parm = &format!("{}/{}", dir, pkg );
    //             let tds_parm = &format!("{}/{}", dir, tds_zip);
    //             let expected_data = &format!("{}/expected.data.sorted", dir);

    //             let before = &format!("{}/before.sh", dir);
    //             if exists_file(dir) {
    //                 run_script(before);
    //             }

    //             Command::main_binary()
    //                 .expect("singe-binary package that compiles")
    //                 .args(&["--no-colors", "-d", d_parm, "-T", tds_parm ])
    //                 .assert()
    //                 .code(1)
    //                 .stdout(predicate::path::eq_file(Path::new(expected_data)));


    //             let after = &format!("{}/after.sh", dir);
    //             if exists_file(after) {
    //                 run_script(after);
    //             }
    //         }
    // }

    macro_rules! tds_errors {
        ($($name:ident,)*) => {
            $(
                #[test]
                fn $name() {
                    let (dir, pkg, tds_zip) =
                        match stringify!($name) {
                            "f0003" => ( "tests/f0003", "somepkg", "somepkg.tds.zip"),
                            "f0004" => ( "tests/f0004", "somepkg", "somepkg.tds.zip"),
                            "f0005" => ( "tests/f0005", "somepkg", "somepkg.zip"),
                            _ => panic!("undefined testcase"), 
                        };

                    let d_parm = &format!("{}/{}", dir, pkg );
                    let tds_parm = &format!("{}/{}", dir, tds_zip);
                    let expected_data = &format!("{}/expected.data.sorted", dir);

                    let before = &format!("{}/before.sh", dir);
                    if exists_file(before) {
                        run_script(before);
                    }

                    Command::main_binary()
                        .expect("singe-binary package that compiles")
                        .args(&["--no-colors", "-d", d_parm, "-T", tds_parm ])
                        .assert()
                        .code(1)
                        .stdout(predicate::path::eq_file(Path::new(expected_data)));


                    let after = &format!("{}/after.sh", dir);
                    if exists_file(after) {
                        run_script(after);
                    }
                }
            )*
        }
    }
    tds_errors!{
        f0003,                  // tds_not_existing,
        f0004,                  // tds zip archive is no zip file
        f0005,                  // tds_wrong_name,
    }

    #[test]
    fn f0001() {
        Command::main_binary()
            .expect("singe-binary package that compiles")
            .args(&["--no-colors" ])
            .assert()
            .code(1)
            .stdout(predicate::path::eq_file(Path::new("tests/f0001/expected.data")));
    }

    #[test]
    fn calling_explain() {
        Command::main_binary()
            .expect("singe-binary package that compiles")
            .args(&["--no-colors", "--explain", "i0001" ])
            .assert()
            .success()
            .stdout(predicate::path::eq_file(Path::new("tests/explain_i0001/expected.data")));
    }

    macro_rules! correct_perms {
        ($($name:ident,)*) => {
            $(
                #[test]
                fn $name() {
                    let e = stringify!($name);
                    let pkg_dir = &format!("tests/{}/somepkg", e);
                    let tds_zip_archive = &format!("tests/{}/somepkg.tds.zip", e);
                    let expected_data_file = &format!("tests/{}/expected.data.sorted", e);
                    let arguments =
                        if e == "ok002" {
                            vec!["--no-colors", "-C", "-d", pkg_dir, "-T", tds_zip_archive ]
                        } else {
                            vec!["--no-colors", "-C", "-d", pkg_dir]
                        };

                    let before = &format!("tests/{}/before.sh", e);
                    if exists_file(before) {
                        run_script(before);
                    }

                    let output = Command::new(main_binary())
                        .args(&arguments)
                        .output()
                        .expect("failed to execute process");

                    let sorted = sort_string_by_lines(&String::from_utf8_lossy(&output.stdout).to_string());


                    assert_eq!(&sorted, &file_contents(expected_data_file));

                    let after = &format!("tests/{}/after.sh", e);
                    if exists_file(after) {
                        run_script(after);
                    }
                }
            )*
        }
    }

    correct_perms!{
        cp_001,
    }

}
