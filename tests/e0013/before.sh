#! /bin/sh

chmod 755 tests/e0013/somepkg/
chmod 644 tests/e0013/somepkg/README
chmod 644 tests/e0013/somepkg/xyz.tex
[[ -e tests/e0013/somepkg/somesocket ]] || \
    python -c "import socket as s; sock = s.socket(s.AF_UNIX); sock.bind('tests/e0013/somepkg/somesocket')"
