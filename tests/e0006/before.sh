#! /bin/sh

chmod 755 tests/e0006/somepkg
chmod 644 tests/e0006/somepkg/*

hidden_dir='tests/e0006/somepkg/.out'
[[ -d $hidden_dir ]] || mkdir -p $hidden_dir
chmod 755 $hidden_dir

info_txt='tests/e0006/somepkg/.out/info.txt'

[[ -f $info_txt ]] || echo "blabla" > $info_txt 

chmod 644 $info_txt
