#! /bin/sh

chmod 755 tests/cp_001/somepkg
chmod 644 tests/cp_001/somepkg/xyz.tex
chmod 644 tests/cp_001/somepkg/README
chmod 700 tests/cp_001/somepkg/doc
chmod 755 tests/cp_001/somepkg/doc/x.pdf
