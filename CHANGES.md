# General remarks

- the version date is the date of upload to CTAN
- the x.y.z is the version of the Rust source code which follows semantic versioning


# Changes


2018-12-09 (1.0.0) 
   - 1.0.0   First stable version

2018-12-16 (1.0.0)
   - quick_intro.txt: improve wording
   - build_ctan_zip.p6: add source files to be included into CTAN zip archive
   - devnotes.md: add documentation how to build your own binary
2018-12-26 (1.1.0)
   - enhance check for generated files which now could reside in any 
     subdirectory in the package tree. This changes the format of the
     e0019 message slightly where now the path of a generated file will be 
     displayed in the e0019 message
   - change ordering of the sections in the pkgcheck.pdf document. First come the
     informational messages, then warnings and error messages and finally fatal messages.
   - add a short info to the PDF documention about how to install pkgcheck 
