#! /usr/bin/perl6

#
# A simple script to package pkgcheck for CTAN
# 

my $base = 'pkgcheck';

#
# file list and permissions
#
my %file_list = (
    "$base" => 0o755,
    "$base/README.md" => 0o644,
    "$base/quick_intro.txt" => 0o644,
    "$base/CHANGES.md" => 0o644,
    "$base/LICENSE-APACHE" => 0o644,
    "$base/LICENSE-MIT" => 0o644,

    "$base/bin" => 0o755,
    "$base/bin/pkgcheck" => 0o755,


    "$base/Cargo.toml" => 0o644,

    "$base/src" => 0o755,
    "$base/src/filemagic.rs" => 0o664,
    "$base/src/generate.pest" => 0o664,
    "$base/src/generate.pest.md" => 0o664,
    "$base/src/gparser.rs" => 0o664,
    "$base/src/linkcheck.rs" => 0o664,
    "$base/src/main.rs" => 0o664,
    "$base/src/messages" => 0o775,
    "$base/src/messages/errorsd.rs" => 0o664,
    "$base/src/messages/fatald.rs" => 0o664,
    "$base/src/messages/informationd.rs" => 0o664,
    "$base/src/messages/mod.rs" => 0o664,
    "$base/src/messages/warningsd.rs" => 0o664,
    "$base/src/recode.rs" => 0o664,
    "$base/src/utils.rs" => 0o664,

    "$base/docs" => 0o755,
    "$base/docs/fatald.tex" => 0o644,
    "$base/docs/errorsd.tex" => 0o644,
    "$base/docs/informationd.tex" => 0o644,
    "$base/docs/pkgcheck.tex" => 0o644,
    "$base/docs/title.tex" => 0o644,
    "$base/docs/warningsd.tex" => 0o644,
    "$base/docs/pkgcheck.pdf" => 0o644,
);

# change directory to one up
chdir '..';


# first unlink the zip file
unlink 'pkgcheck.zip';

#
# set permissions accordingly
#
for %file_list.keys.sort  -> $f  {
    my $p = %file_list{$f};

    printf("chmod %#o $f\n", $p);
    chmod $p, $f;
}

my @files = %file_list.keys;


my $proc = run(
    '/usr/bin/zip', 'pkgcheck.zip', @files,
    :out, :err, :merge
);

put "Zipping the files for the CTAN package";
put "{$proc.out.slurp}";
